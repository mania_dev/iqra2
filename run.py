from flask import Flask, g
from core.views import main_app
from core.helper import utils
from core.data_access.user_dao import UserDAO
from flask_jsglue import JSGlue
from flask_login import (LoginManager, login_required, logout_user, 
        login_user, current_user)

app = Flask(__name__, instance_relative_config=True, static_url_path=None)
app.register_blueprint(main_app)
app.register_blueprint(utils)

login_manager = LoginManager()

login_manager.init_app(app)
JSGlue(app)

# the callback function that 
# returns the userDAO
@login_manager.user_loader
def load_user(userid):
    userDAO = UserDAO()
    return userDAO.get(userid)

if __name__ == '__main__':
    app.config.from_object('config')
    app.config.from_pyfile('config.py')
    
    app.static_url_path=app.config.get('STATIC_FOLDER')
    app.static_folder=app.root_path + app.static_url_path

    # app.config['UPLOAD_FOLDER'] = '/uploads/'
    # app.config['MAX_CONTENT_PATH'] = app.config.get('MAX_CONTENT_PATH')

    print('Static URL path -->', app.static_url_path)
    print('Static URL folder -->', app.static_folder)
    
    print(app.url_map)
    print(app.config.get('HOST'))
    print(app.config.get('PORT'))

    app.run(
        host=app.config.get('HOST'),
        port=app.config.get('PORT'),
        threaded=True
        )