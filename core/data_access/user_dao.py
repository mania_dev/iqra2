from core.models.user import User
from flask_login import (LoginManager, login_required, logout_user, 
        login_user, current_user)
from flask import session, g
import core.query as query

class UserDAO:

    def __init__(self):
        self.valid_users = []

    def get(self, username):
        user = User(username)
        return user

    def validate(self, username, password):

        # fetch the school_fk
        validation_response = query.get__public__verify_login_user(username, password)
        school_fk = validation_response[0]
        print(school_fk)

        # if there was a school_fk found
        if school_fk != None:
            
            # set session variables
            session['school_fk'] = school_fk
            
        print("school_fk from the val func:", school_fk)    
        return school_fk