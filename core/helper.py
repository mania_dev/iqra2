import psycopg2, csv, os
from flask import current_app, Blueprint, session, g

utils = Blueprint('utils', __name__)

# processes a csv file
def process_csv(filename):

    with open(os.path.join(current_app.instance_path, current_app.config['UPLOAD_FOLDER'], filename), newline='\n') as File:
        reader = csv.reader(File)
        for row in reader:
            print(row)

# returns a connection object
def connect_db():
    # return psycopg2.connect("dbname='iqradb' user='anees' password='111@torchdevice' host='localhost'")
    dbname=current_app.config.get('DB_NAME')
    user=current_app.config.get('DB_USER')
    password=current_app.config.get('DB_PASSWORD')
    host=current_app.config.get('DB_HOST')
    return psycopg2.connect(dbname=dbname, user=user, password=password, host=host)


def get_connection():
    print("creating a connection")
    if not hasattr(g, 'db'):
        g.db = connect_db()
    return g.db    


# after a request
# @utils.teardown_app_request
def tear_db_connection():
    print("tearing connection")
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()
