from flask import current_app, Blueprint, request, session, g, redirect, url_for, abort, render_template, flash, jsonify
from flask_login import (LoginManager, login_required, logout_user, 
        login_user, current_user)
from werkzeug import secure_filename
from core.data_access.user_dao import UserDAO
import core.query as query
import core.helper as helper
import datetime, json, os
main_app = Blueprint('main_app', __name__, template_folder='templates')

# ---------------
# login page
# ---------------
@main_app.route('/')
def index(name=None):

    # if the user has not logged in
    if 'user_id' not in session:
        return render_template('login.html')

    # if a session is active, we need to send them to home
    return home()

# ---------
# home page
# ---------
@main_app.route('/home')
def home():

    school_parameters = {
        "school_fk" : session['school_fk']
    }
    school_record = query.get__public__get_school_details(**school_parameters)
    # render home page
    return render_template('home.html', school_record = school_record)

# ----------------
# renders student list page
# ----------------
@main_app.route('/student_list')
@login_required
def student_list():

    return render_template('students/student_list.html')


# ----------------
# renders group list page
# ----------------
@main_app.route('/groups')
@login_required
def group_list():

    school_fk = session['school_fk']
    groups = query.get__public__get_all_groups(school_fk)
    return render_template('group/group_list.html', group_list = groups)

# ----------------
# renders edit group page
# ----------------
@main_app.route('/groups/add_edit_group/<group_fk>')
@login_required
def edit_group(group_fk):

    get_group_params = {
        "school_fk" : session['school_fk'],
        "group_fk"  : group_fk
    }
    group_record = query.get__public__get_group_record(**get_group_params)
    return render_template('group/add_edit_group.html', group_record = group_record)

# ----------------
# renders group list page
# ----------------
@main_app.route('/groups/add_edit_group/new')
@login_required
def add_group():

    # we must pass an empty student record, because the template expects one
    # we pass an empty object with a recid = -1
    group_record = {
        "recid" : -1
    }

    return render_template('group/add_edit_group.html', group_record = group_record)

# ----------
# save group
# ----------
@main_app.route('/groups/save_group/<group_fk>', methods=['POST'])
@login_required
def save_group(group_fk):

    group_record = {
        "pk"                : group_fk,
        "school_fk"         : session['school_fk'],
        "group_name"        : request.form['group_name'],
        "group_description" : request.form['group_description']
    }

    query.put__public__add_edit_group(**group_record)

    return redirect(url_for('main_app.group_list'))

# -----------------
# save entered student details
# -----------------
@main_app.route('/student_details/save_student/<student_fk>', methods=['POST'])
@login_required
def save_student(student_fk):

    # format the address as a json
    address = {
        "line_one" 	: request.form['address_line_1'],
        "line_two" 	: request.form['address_line_2'],
        "postcode"	: request.form['post_code']
    }

    surgery_address = {
        "line_one"  : request.form['surgery_address_line_1'],
        "line_two" 	: request.form['surgery_address_line_2'],
        "postcode"	: request.form['post_code']
    }

    # format the dates
    dob = request.form['dob']
    dob = dob.split('.')
    dob.reverse()
    dob = '-'.join(dob)

    join_date = request.form['join_date']
    join_date = join_date.split('.')
    join_date.reverse()
    join_date = '-'.join(join_date)

    # medical_info, doctor_name, surgery_address, surgery_telephone, notes
    # build student record
    student_record = {
        "pk"				: student_fk,
        "school_fk"			: session['school_fk'],
        "first_name" 		: request.form['first_name'],
        "last_name" 		: request.form['last_name'],
        "gender" 			: request.form['gender'],
        "home_telephone" 	: request.form['home_telephone'],
        "email_address" 	: request.form['email_address'],
        "father_mobile"     : request.form['father_mobile'], 
        "mother_mobile"     : request.form['mother_mobile'], 
        "guardian_mobile"   : request.form['guardian_mobile'], 
        "join_date"         : join_date,
        "dob"	 			: dob,
        "address"           : address,
        "medical_info" 		: request.form['medical_info'],
        "status" 			: request.form['status'],
        "doctor_name"       : request.form['doctor_name'], 
        "surgery_address"   : surgery_address, 
        "surgery_telephone" : request.form['surgery_telephone'], 
        "notes"             : request.form['notes']
    }

    # call query to add student
    query.put__public__add_edit_student(**student_record)

    # print(pk, school_fk, first_name, last_name, gender, email_address, mobile_number, dob, address_line_1, address_line_2, post_code, dietary_info, status)
    return redirect(url_for('main_app.student_list'))

# -----------------------
# get student details to populate
# student details page
# -----------------------
@main_app.route('/student_details/<student_fk>')
@login_required
def student_details(student_fk):

    school_fk = session['school_fk']
    get_student_params = {
        "school_fk"  : school_fk,
        "pk" : student_fk
    }

    groups = query.get__public__get_all_groups(school_fk)
    student_record = query.get__public__get_student(**get_student_params)
    print(student_record)
    return render_template('students/student_details.html', student_record = student_record, groups = groups)

# -----------------
# renders add new student page
# -----------------
@main_app.route('/student_details/new')
@login_required
def add_new_student():

    # we must pass an empty student record, because the template expects one
    # we pass an empty object with a recid = -1
    student_record = {
        "recid" : -1
    }

    return render_template('students/student_details.html', student_record = student_record)

# ----------------
# student list api
# ----------------
@main_app.route('/api/get_all_students', methods=['POST'])
def api__get_all_students():

    params = {
        "school_fk" : session['school_fk']
    }

    student_records = query.get__public__get_all_students(**params)
    # print(student_records)
    return jsonify(records = student_records)
    # return json.dumps(student_records)

# ----------------
# whoami to get current user_id
# ----------------
@main_app.route("/whoami")
def whoami():
    user_id = current_user.get_id()
    if user_id == None:
        return "No one logged in"
    else:
        return user_id

# ----------------
# on submit of username and password
# ----------------
@main_app.route("/validate_login", methods=['POST'])
def validate_login():

    username = request.form['username']
    password = request.form['password']

    if username == None:
        return "Need user_name"
    else:
        userDAO = UserDAO()

        # validate the username and password
        school_fk = userDAO.validate(username, password)
        if school_fk is not None:
            
            # to log this user in, we create a user object
            # and pass it to login_user function
            login_user(userDAO.get(username), remember=True)
            
            print('successfully logged in')
            print("g bhai %s and %s" %(session['school_fk'], session['user_id']))
            return redirect(url_for('main_app.home'))

        return "Invalid user_name"

# ----------------
# uploaded file processor
# ----------------
@main_app.route("/student_file_processor")
@login_required
def student_file_processor():

    return None


# -----------------
# csv upload target
# -----------------
@main_app.route("/uploader", methods = ['GET', 'POST'])
@login_required
def uploader():
    
    if request.method == 'POST':
        f = request.files['file']
        filename = secure_filename(f.filename)
        f.save(os.path.join(current_app.instance_path, current_app.config['UPLOAD_FOLDER'], filename))

        helper.process_csv(filename)
    
        return 'file uploaded'

# -----------------
# csv upload page
# -----------------
@main_app.route("/student_upload")
@login_required
def student_upload():

    return render_template("student_upload.html")

# ----------------
# user logout endpoint
# ----------------
@main_app.route("/logout")
@login_required
def logout():

    # loop the session
    for key in session.keys():
        print("%s : %s" %(key, session[key]))

    # remove the session vars
    session.pop('school_fk', None)

    # log the user out
    logout_user()

    # tear the db connection
    helper.tear_db_connection()

    print("successfully logge out\n")
    for key in session.keys():
        print("%s : %s" %(key, session[key]))

    return redirect(url_for('main_app.index'))