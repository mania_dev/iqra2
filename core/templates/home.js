// ********************
// Body onload function
// ********************
$(document).ready(function(){

    var school_record = {{school_record|safe}};

    // store the    school_record object in session variable. dont  need the ark i guess
    var school_details = {};
    school_details.school_fk = school_record.recid;
    school_details.name = school_record.rec_name;
    school_details.address = school_record.rec_address;
    school_details.email_address = school_record.rec_email_address;
    school_details.phone_number = school_record.rec_phone_number;
    school_details.web_url = school_record.rec_web_url;
    school_details.charity_number = school_record.charity_number;
    
    sessionStorage.setItem( "ark_session", JSON.stringify(school_details) );

    // we need to build the row contents
    $.each(school_details, function(key, value) {
        
        var row;
        if(key === 'name') { 
            row = '<tr><td>Madrasah Name:</td><td>' + value + '</td></tr>';
        } else if (key === 'school_fk') {
            row = '<tr><td>School Id:</td><td>' + value + '</td></tr>';
        } else if (key === 'address'){
            row = '<tr><td>Address:</td><td>' + Object.values(school_details.address).join('<br>') + '</td></tr>';
        } else if (key === 'email_address') {
            row = '<tr><td>Email:</td><td>' + value + '</td></tr>';
        } else if (key === 'phone_number') {
            row = '<tr><td>Phone:</td><td>' + value + '</td></tr>';
        } else if (key === 'email_address') {
            row = '<tr><td>Email:</td><td>' + value + '</td></tr>';
        } else if (key === 'web_url') {
            row = '<tr><td>Web:</td><td>' + value + '</td></tr>';
        } else if (key === 'charity_number') {
            row = '<tr><td>Charity Number:</td><td>' + value + '</td></tr>';
        } 

        $('#school_details > tbody').append(row);

    });
})