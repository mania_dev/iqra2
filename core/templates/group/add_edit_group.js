// create the wrapper
var add_edit_group = (function(){

var public = {};

// *****************
// Set group details
// *****************
public.set_group_details = function(group_record){
    $('#group_name').val(group_record.rec_group_name);
    $('#group_description').text(group_record.rec_description);
}

// *****************
// Save group details
// *****************
public.save_record = function(){

    let group_form = document.getElementById("group_form");
    let group_record = {{group_record|safe}};

    group_form.action = Flask.url_for('main_app.save_group', {group_fk : group_record.recid});
    group_form.submit();
}

return {
    set_group_details : public.set_group_details,
    save_record       : public.save_record
};

})();


// ********************
// Body onload function
// ********************
$(document).ready(function(){

    let group_record = {{group_record|safe}};

    if (group_record.recid != -1) {
        add_edit_group.set_group_details(group_record);
    }


    $("#back_button").on("click", function(){
        window.location.href = Flask.url_for('main_app.group_list');
    });

    // ************************
    // Form validation settings
    // ************************
    $('form')
        .form({
            on: 'blur',
            fields: {
                group_name: {
                    identifier  : 'group_name',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter a group name.'
                        }
                    ]
                },

                group_description: {
                    identifier  : 'group_description',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter a group description.'
                        }
                    ]
                }
            },
            
            onSuccess: function() {

                add_edit_group.save_record();
                return false; // false is required if you do don't want to let it submit
            }
        });
    
})