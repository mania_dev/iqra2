// ********************
// Body onload function
// ********************
$(document).ready(function(){

    $(".ui.pointing.menu a").removeClass('active');    
    $("#tab-groups").addClass('active');

    var group_list = {{group_list|safe}};
    console.log(group_list)

    var groups_html = [];
    for(let i = 0; i < group_list.length; i = i + 1) {

        groups_html.push("<div class='item'>");
        groups_html.push("<div class='right floated content'><div id=group_" + group_list[i].recid + " class='ui button'>Edit<\/div><\/div>");
        groups_html.push("<img class=\"ui avatar image\" src=\"{{url_for('static', filename='images/Group-icon.png')}}\">");
        groups_html.push("<div class=content>" + group_list[i].rec_group_name + " - " + group_list[i].rec_description + "<\/div><\/div>");
        groups_html.push("<\/div>")
    }

    $("#group_list").html(groups_html.join(""));

    // ---------------------
    // Button redirects
    // ---------------------

    // add new
    $('#new_group').on('click', function(){
        window.location.href = Flask.url_for("main_app.add_group");
    });

    // edit group
    $('.ui.button').on('click', function(){
        let group_fk = this.id.match(/\d/)[0];
        window.location.href = Flask.url_for("main_app.edit_group", {group_fk : group_fk});
    });



})

/*
<div class="item">
<div class="right floated content">
    <div class="ui button">Edit</div>
</div>
<img class="ui avatar image" src="{{url_for('static', filename='images/Group-icon.png')}}">
<div class="content">
    Lena
</div>
</div>
*/