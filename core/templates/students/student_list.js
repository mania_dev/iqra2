// onload function
$(document).ready(function () {

    $(".ui.pointing.menu a").removeClass('active');    
    $("#tab-students").addClass('active');    

    // -----------------------------
    // edit student onclick function
    // -----------------------------
    $('#edit_student').click(function () {

        // get the student_fk of the selected student
        var student_fk = w2ui['student_list'].getSelection()[0];

        // if a student was selected
        if (student_fk !== undefined) {
            window.location.href = Flask.url_for("main_app.student_details", {student_fk : student_fk} );
        } else {
            // alert the user
            $('#select_student_modal')
                .modal('show');
        }

    });

    // ----------------------------
    // add student onclick function
    // ----------------------------
    $('#add_student').click(function() {

        window.location.href = Flask.url_for("main_app.add_new_student")
    });

    // ------------------
    // student list grid
    // ------------------
    $('#student_list').w2grid({
        name: 'student_list',
        header: 'List of Names',
        show: {
            toolbar: true,
            footer: true
        },
        columns: [
            { field: 'recid', caption: 'ID', size: '50px', sortable: true, attr: 'align=center' },
            { field: 'rec_last_name', caption: 'Last Name', size: '30%', sortable: true, resizable: true },
            { field: 'rec_first_name', caption: 'First Name', size: '30%', sortable: true, resizable: true },
            { field: 'rec_email_address', caption: 'Email', size: '40%', resizable: true },
            { field: 'rec_create_date', caption: 'Start Date', size: '120px', resizable: true },
        ],
        searches: [
            { field: 'rec_last_name', caption: 'Last Name', type: 'text' },
            { field: 'rec_first_name', caption: 'First Name', type: 'text' },
            { field: 'rec_email_address', caption: 'Email', type: 'text' },
        ],
        sortData: [{ field: 'recid', direction: 'ASC' }],
        url: Flask.url_for("main_app.api__get_all_students")
    });

});