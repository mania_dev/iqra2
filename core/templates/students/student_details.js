// create the wrapper
var student_details = (function() {

// global vars
var student_record;
var groups;

var public = {};
// ******************************
// Populate the student details 
// ******************************
public.set_student_details = function(student_record, groups){

    // set all of the details in
    $("#first_name").val(student_record.rec_first_name);
    $("#last_name").val(student_record.rec_last_name);
    $("#group").dropdown('set text', student_record['rec_group_name']);
    $("#home_telephone").val(student_record.rec_home_telephone);
    $("#email_address").val(student_record.rec_email_address);
    $("#gender").dropdown('set selected', student_record['rec_gender']);

    $('#father_mobile').val(student_record.rec_father_mobile);
    $('#mother_mobile').val(student_record.rec_mother_mobile);
    $('#guardian_mobile').val(student_record.rec_guardian_mobile);

    $("#address_line_1").val(student_record.rec_address.line_one);
    $("#address_line_2").text(student_record.rec_address.line_two);
    $("#post_code").val(student_record.rec_address.postcode);

    $("#medical_info").val(student_record.rec_medical_info);
    $('#doctor_name').val(student_record.rec_doctor_name);
    $('#surgery_address_line_1').val(student_record.rec_surgery_address.line_one)
    $('#surgery_address_line_2').text(student_record.rec_surgery_address.line_two)
    $('#surgery_post_code').val(student_record.rec_surgery_address.postcode)
    $('#surgery_telephone').val(student_record.rec_surgery_telephone)

    // format the way date picker needs it
    $("#dob").val(ui.get_date_picker_format(student_record.rec_dob));
    $("#join_date").val(ui.get_date_picker_format(student_record.rec_join_date));

    $("#status").dropdown('set selected', student_record['rec_status']);
    $('#notes').text(student_record.rec_notes)

}

// ************************
// Save the student details
// ************************
public.save_record = function(e) {
    
    // get the form  
    let student_form = document.getElementById('student_form');
    let student_record = {{student_record|safe}};

    // set the form action with the student pk
    // student_form.action="save_student/" + student_record.recid;
    student_form.action = Flask.url_for("main_app.save_student", {student_fk : student_record.recid} );
    student_form.submit();
}

// ************************
// Setting the group values
// ************************
public.set_group_dropdown = function(){
    
    let groups = {{groups|safe}};
    
    groups.forEach(group => {
        let item = "<div class=item data-value=" + group.recid + ">" + group.rec_group_name + "<\/div>";
        $("#group > .menu").append(item);
    });

}

return {
    set_group_dropdown  : public.set_group_dropdown,
    set_student_details : public.set_student_details,
    save_record         : public.save_record
}
})();

// ***********************
// Body onready function
// ***********************
$(document).ready( function() {

    // get the student details
    let student_record = {{student_record|safe}};
    let groups = {{groups|safe}};
    
    // if this is an existing record
    if (student_record.recid != -1) {
        
        // set dropdown values
        student_details.set_group_dropdown();

        // set the values in their placeholders
        student_details.set_student_details(student_record, groups);
    }

    // initialise the ui elements
    $('input[type=eu-date]').w2field('date',  
        { format: 'd.m.yyyy' }
    );

    $('#group')
        .dropdown();

    $('#gender')
        .dropdown();

    $('#status') 
        .dropdown();

    // ---------------------
    // Close button redirect
    // ---------------------
    $('#close').on('click', function(){
        window.location.href = Flask.url_for("main_app.student_list")
    });


    // ************************
    // Form validation settings
    // ************************
    $('form')
        .form({
            on: 'blur',
            fields: {
                first_name: {
                    identifier  : 'first_name',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter a first name.'
                        }
                    ]
                },

                last_name: {
                    identifier  : 'last_name',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter a last name.'
                        }
                    ]
                },

                gender: {
                    identifier  : 'gender',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please select the gender.'
                        }
                    ]
                },
                
                status: {
                    identifier  : 'status',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please select the status.'
                        }
                    ]
                },

                home_telephone: {
                    identifier  : 'home_telephone',
                    rules: [
                        {
                            // type   : 'regExp[/^[\\+][4][4][\\d]{10}$/g]',
                            // prompt : 'Please enter a valid mobile number. (Starting with +44, followed by 10 numbers)'
                            type   : 'regExp[/^[\\+]*[\\d]{5,14}$/g]',
                            prompt : 'Please enter a valid telephone number.'
                        }
                    ]
                },

                father_mobile: {
                    identifier  : 'father_mobile',
                    rules: [
                        {
                            type   : 'regExp[/^[\\+]*[\\d]{5,14}$/g]',
                            prompt : 'Please enter a valid telephone number.'
                        }
                    ]
                },

                mother_mobile : {
                    identifier  : 'mother_mobile',
                    rules: [
                        {
                            type   : 'regExp[/^[\\+]*[\\d]{5,14}$/g]',
                            prompt : 'Please enter a valid telephone number.'
                        }
                    ]
                },

                guardian_mobile: {
                    identifier  : 'guardian_mobile',
                    rules: [
                        {
                            type   : 'regExp[/^[\\+]*[\\d]{5,14}$/g]',
                            prompt : 'Please enter a valid telephone number.'
                        }
                    ]
                },

                surgery_telephone: {
                    identifier  : 'surgery_telephone',
                    rules: [
                        {
                            type   : 'regExp[/^[\\+]*[\\d]{5,14}$/g]',
                            prompt : 'Please enter a valid telephone number.'
                        }
                    ]
                },
            },


            onSuccess: function() {

                student_details.save_record();
                return false; // false is required if you do don't want to let it submit
            }
    });


});