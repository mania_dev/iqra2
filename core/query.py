from flask import current_app, Blueprint, request, session, g, redirect, url_for, abort, render_template, flash
from core import helper
import json
import datetime

def format_records(data, columns):
    
    list_records = []
    columns = [column.replace('rec_pk', 'recid') for column in columns ]
    for record in data:
        row_object = {}
        
        # we start to build the row object
        for index, value in enumerate(record):

            # format the date as strings
            if type(value) is datetime.datetime or type(value) is datetime.date :
                value = value.strftime('%d %b %Y')
            
            # format 'None' as empty string
            if value == None:
                value = ''

            row_object[columns[index]] = value
        list_records.append(row_object)
    
    return list_records
    

# data = [(7, datetime.datetime(2016, 5, 28, 15, 38, 41, 827905), datetime.datetime(2016, 5, 28, 15, 38, 41, 827905), 'Abdullah', 'Omar', '0744860', 'aneesmania@gmail.com', 'g', datetime.date(1988, 9, 1), {'street': 'East Park Road', 'number': '249', 'postcode': 'le5 5hj', 'city': 'Leicester'}, 'nothing'), (8, datetime.datetime(2016, 5, 28, 15, 39, 8, 247280), datetime.datetime(2016, 5, 28, 15, 39, 8, 247280), 'Abdullah', 'Omar', '0744860', 'aneesmania@gmail.com', 'g', datetime.date(1988, 9, 1), {'street': 'East Park Road', 'number': '249', 'postcode': 'le5 5hj', 'city': 'Leicester'}, 'nothing')]
# columns = ['rec_pk', 'rec_create_date', 'rec_modified_date', 'rec_first_name', 'rec_last_name', 'rec_mobile_number', 'rec_email_address', 'rec_status', 'rec_dob', 'rec_address', 'rec_dietary_info']

# format_records(data,columns)
# var_pk, var_school_fk, var_first_name, var_last_name, var_gender, var_home_telephone, var_email_address, var_father_mobile character varying doctor_name, surgery_address, surgery_telephone, notes
def put__public__add_edit_student(pk, school_fk, first_name, last_name, gender, home_telephone, email_address, father_mobile, mother_mobile, guardian_mobile, join_date, status, dob, address, medical_info, doctor_name, surgery_address, surgery_telephone, notes):
    
    # returns the g.db object
    db_connection = helper.get_connection()
    cur = db_connection.cursor()
    # cur.execute("SELECT * FROM student;")
    
    cur.callproc("put__public__add_edit_student", [pk, school_fk, first_name, last_name, gender, home_telephone, email_address, father_mobile, mother_mobile, guardian_mobile, join_date, status, dob, json.dumps(address), medical_info, doctor_name, json.dumps(surgery_address), surgery_telephone, notes, ])
    cur.close()
    db_connection.commit()
    # db_connection.close()

def put__public__add_edit_group(pk, school_fk, group_name, group_description):

    # returns the g.db object
    db_connection = helper.get_connection()
    cur = db_connection.cursor()
    
    cur.callproc("put__public__add_edit_group", [pk, school_fk, group_name, group_description])
    cur.close()
    db_connection.commit()    

def get__public__get_all_groups(school_fk):

    db_connection = helper.get_connection()

    cur = db_connection.cursor()
    cur.callproc("get__public__get_all_groups", [school_fk, ])
    
    headers = [desc[0] for desc in cur.description]
    groups = cur.fetchall()
    groups = format_records(groups, headers)

    cur.close()
    db_connection.commit()

    return groups

def get__public__get_group_record(school_fk, group_fk):

    db_connection = helper.get_connection()

    cur = db_connection.cursor()
    cur.callproc("get__public__get_group_record", [school_fk, group_fk])
    
    headers = [desc[0] for desc in cur.description]
    group_record = cur.fetchall()

    cur.close()
    db_connection.commit()

    group_record = format_records(group_record, headers)[0]

    print(group_record)

    return group_record


def get__public__get_student(school_fk, pk):

    db_connection = helper.get_connection()

    cur = db_connection.cursor()
    cur.callproc("get__public__get_student", [school_fk, pk, ])
    
    headers = [desc[0] for desc in cur.description]
    student_record = cur.fetchall()

    cur.close()
    db_connection.commit()

    # there will only be one record to fetch
    print(student_record, headers)
    student_record = format_records(student_record, headers)[0]

    return student_record

def get__public__get_all_students(school_fk):

    db_connection = helper.get_connection()

    cur = db_connection.cursor()

    cur.callproc("get__public__get_all_students", [school_fk, ])
    print("them students")
    headers = [desc[0] for desc in cur.description]
    students = cur.fetchall()
    # print(colnames)
    
    cur.close()
    db_connection.commit()

    return format_records(students, headers)

def get__public__get_school_details(school_fk):

    db_connection = helper.get_connection()

    cur = db_connection.cursor()

    cur.callproc("get__public__get_school_details", [school_fk, ])

    headers = [desc[0] for desc in cur.description]
    school_record = cur.fetchall()
    

    cur.close()
    db_connection.commit()

    # there will only be one record to fetch
    school_record = format_records(school_record, headers)[0]
    print(school_record)

    return school_record

def get__public__verify_login_user(username, password):

    db_connection = helper.get_connection()
    cur = db_connection.cursor()

    cur.callproc("get__public__verify_login_user", [username, password, ])
    school_fk = cur.fetchone()

    cur.close()
    db_connection.commit()
    
    return school_fk