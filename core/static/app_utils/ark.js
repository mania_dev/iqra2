var ark_session = function(pk, name, address, email_address, phone_number, web_url, charity_number){

    var public = {};

    public.get_school_fk = function(){
        return pk;
    },

    public.get_name = function(){
        return name;
    },

    public.get_address = function(){
        return address;
    },

    public.get_email = function(){
        return email_address;
    },

    public.get_phone_number = function(){
        return phone_number;
    },

    public.get_web_url = function(){
        return web_url;
    },

    public.get_charity_number = function(){
        return charity_number;
    }

    return {
        get_school_fk: public.get_school_fk,
        get_name: public.get_name,
        get_address: public.get_address,
        get_email: public.get_email,
        get_phone_number: public.get_phone_number,
        get_web_url: public.get_web_url,
        get_charity_number: public.get_charity_number
    }
};