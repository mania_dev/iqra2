var ui = (function() {

var public = {};

//-----------------
// format date for date picker
// ----------------
public.get_date_picker_format = function(date) {

    // create the date object
    date = new Date(date);
    var month = date.getMonth() + 1;


    // format and return the date
    return date.getDate() + "." + month + "." + date.getUTCFullYear()
}

return {
    get_date_picker_format : public.get_date_picker_format
}
})();